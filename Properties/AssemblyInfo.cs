﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("StudioKit.Caching")]
[assembly: AssemblyDescription("Wrapper functions for caching.  Currently implemented via Memcached")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Purdue University")]
[assembly: AssemblyProduct("StudioKit.Caching")]
[assembly: AssemblyCopyright("Copyright ©Purdue University  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("46eb1b9b-a16a-42fa-985c-f6c28df8bfbb")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("1.3.0")]
[assembly: AssemblyInformationalVersion("1.3.0")]