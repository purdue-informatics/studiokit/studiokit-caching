﻿using StackExchange.Redis;
using StudioKit.Caching.Interfaces;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.Sharding.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Caching.Redis;

public class RedisClient : IAsyncCacheClient
{
	private readonly RedisConnectionFactory _redisConnectionFactory;
	private readonly IErrorHandler _errorHandler;
	private readonly string _shardKey;

	public RedisClient(
		RedisConnectionFactory redisConnectionFactory,
		IErrorHandler errorHandler,
		IShardKeyProvider shardKeyProvider = null,
		string shardKey = null)
	{
		_redisConnectionFactory = redisConnectionFactory ?? throw new ArgumentNullException(nameof(redisConnectionFactory));
		_errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
		_shardKey = !string.IsNullOrWhiteSpace(shardKey) ? shardKey : shardKeyProvider?.GetShardKey();
		// either shardKey or shardKeyProvider is required
		if (string.IsNullOrWhiteSpace(_shardKey))
		{
			throw new ArgumentNullException(nameof(shardKeyProvider));
		}
	}

	#region IAsyncCacheClient

	public async Task<object> GetAsync(string key)
	{
		try
		{
			return await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
				$"{nameof(IDatabase.StringGetAsync)}({key})",
				async (db, _) => Deserialize<object>(await db.StringGetAsync(key)));
		}
		catch (Exception ex)
		{
			_errorHandler.CaptureException(ex);
			return null;
		}
	}

	public async Task<T> GetAsync<T>(string key)
	{
		try
		{
			return await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
				$"{nameof(IDatabase.StringGetAsync)}({key})",
				async (db, _) => Deserialize<T>(await db.StringGetAsync(key)));
		}
		catch (Exception ex)
		{
			_errorHandler.CaptureException(ex);
			return default;
		}
	}

	public async Task<IEnumerable<KeyValuePair<string, object>>> GetManyAsync(IEnumerable<string> keys)
	{
		var keysList = keys as IList<string> ?? keys.ToList();
		if (keysList.Count == 0) return new List<KeyValuePair<string, object>>();
		var redisKeys = keysList.Select(key => new RedisKey(key)).ToArray();
		var values = await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.StringGetAsync)}([{string.Join(",", keysList)}])",
			async (db, _) => await db.StringGetAsync(redisKeys));
		return keysList.Zip(values,
			(s1, s2) => new KeyValuePair<string, object>(s1, s2.IsNullOrEmpty ? null : Deserialize<object>(s2)));
	}

	public async Task<bool> PutAsync(string key, object value)
	{
		return await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.StringSetAsync)}({key})",
			async (db, _) => await db.StringSetAsync(key, Serialize(value)));
	}

	public async Task<bool> PutAsync(string key, object value, TimeSpan validFor)
	{
		return await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.StringSetAsync)}({key}, value, {validFor})",
			async (db, _) => await db.StringSetAsync(key, Serialize(value), validFor));
	}

	public async Task<long> IncrementAsync(string key, long delta, long initialValue)
	{
		var exists = await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.KeyExistsAsync)}({key})",
			async (db, _) => await db.KeyExistsAsync(key));
		if (!exists)
			await PutAsync(key, initialValue);
		return await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.StringIncrementAsync)}({key})",
			async (db, _) => await db.StringIncrementAsync(key, delta));
	}

	public async Task<long> DecrementAsync(string key, long delta, long initialValue)
	{
		var exists = await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.KeyExistsAsync)}({key})",
			async (db, _) => await db.KeyExistsAsync(key));
		if (!exists)
			await PutAsync(key, initialValue);
		return await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.StringIncrementAsync)}({key})",
			async (db, _) => await db.StringDecrementAsync(key, delta));
	}

	public async Task<bool> RemoveAsync(string key)
	{
		return await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.KeyDeleteAsync)}({key})",
			async (db, _) => await db.KeyDeleteAsync(key));
	}

	public async Task PurgeByPrefixAsync(string prefix)
	{
		var keysToDelete = await GetKeysAsync($"{prefix}*");
		if (keysToDelete.Length == 0) return;
		await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.KeyDeleteAsync)}([{string.Join(",", keysToDelete)}])",
			async (db, _) => await db.KeyDeleteAsync(keysToDelete));
	}

	public async Task PurgeByPrefixListAsync(IEnumerable<string> prefixEnumerable)
	{
		var keysToDelete = new List<RedisKey>();
		foreach (var prefix in prefixEnumerable)
		{
			var keys = await GetKeysAsync($"{prefix}*");
			keysToDelete.AddRange(keys);
		}

		var keysToDeleteArray = keysToDelete.Distinct().ToArray();
		if (keysToDeleteArray.Length == 0) return;

		await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.KeyDeleteAsync)}([{string.Join(",", keysToDeleteArray)}])",
			async (db, _) => await db.KeyDeleteAsync(keysToDeleteArray));
	}

	public async Task PurgeByContainsAsync(string contains)
	{
		var keysToDelete = await GetKeysAsync($"*{contains}*");
		if (keysToDelete.Length == 0) return;
		await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.KeyDeleteAsync)}([{string.Join(",", keysToDelete)}])",
			async (db, _) => await db.KeyDeleteAsync(keysToDelete));
	}

	public async Task PurgeByContainsListAsync(IEnumerable<string> containsEnumerable)
	{
		var keysToDelete = new List<RedisKey>();
		foreach (var contains in containsEnumerable)
		{
			var keys = await GetKeysAsync($"*{contains}*");
			keysToDelete.AddRange(keys);
		}

		var keysToDeleteArray = keysToDelete.Distinct().ToArray();
		if (keysToDeleteArray.Length == 0) return;

		await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IDatabase.KeyDeleteAsync)}([{string.Join(",", keysToDeleteArray)}])",
			async (db, _) => await db.KeyDeleteAsync(keysToDeleteArray));
	}

	public async Task FlushAsync()
	{
		await _redisConnectionFactory.ExecuteWithRetryAsync<Task>(_shardKey,
			nameof(IServer.FlushDatabaseAsync),
			async (_, server) =>
			{
				await server.FlushDatabaseAsync();
				return null;
			});
	}

	#endregion IAsyncCacheClient

	#region Helpers

	private async Task<RedisKey[]> GetKeysAsync(string pattern, CancellationToken cancellationToken = default)
	{
		return await _redisConnectionFactory.ExecuteWithRetryAsync(_shardKey,
			$"{nameof(IServer.KeysAsync)}({pattern})",
			async (_, server) =>
			{
				var keys = new List<RedisKey>();
				await foreach (var key in server.KeysAsync(pattern: new RedisValue(pattern)).WithCancellation(cancellationToken))
				{
					keys.Add(key);
				}

				return keys.ToArray();
			}, cancellationToken);
	}

	private static byte[] Serialize(object o)
	{
		if (o == null)
		{
			return null;
		}

		var serializedObject = JsonSerializer.Serialize(o);
		using var memoryStream = new MemoryStream();
		using var binaryWriter = new BinaryWriter(memoryStream);
		binaryWriter.Write(serializedObject);
		var objectDataAsStream = memoryStream.ToArray();
		return objectDataAsStream;
	}

	private static T Deserialize<T>(byte[] stream)
	{
		if (stream == null)
		{
			return default;
		}

		using var memoryStream = new MemoryStream(stream);
		using var binaryReader = new BinaryReader(memoryStream);
		var jsonResult = binaryReader.ReadString();
		var result = JsonSerializer.Deserialize<T>(jsonResult);
		return result;
	}

	#endregion Helpers
}