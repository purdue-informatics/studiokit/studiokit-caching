using Microsoft.Extensions.Logging;
using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using StackExchange.Redis;
using System;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Caching.Redis;

/// <summary>
/// See https://github.com/Azure-Samples/azure-cache-redis-samples/blob/main/quickstart/dotnet-core/RedisConnection.cs
/// </summary>
public class RedisConnection : IDisposable
{
	// StackExchange.Redis will also be trying to reconnect internally,
	// so limit how often we recreate the ConnectionMultiplexer instance
	// in an attempt to reconnect
	private readonly TimeSpan _reconnectMinInterval = TimeSpan.FromSeconds(60);

	// If errors occur for longer than this threshold, StackExchange.Redis
	// may be failing to reconnect internally, so we'll recreate the
	// ConnectionMultiplexer instance
	private readonly TimeSpan _reconnectErrorThreshold = TimeSpan.FromSeconds(30);
	private readonly TimeSpan _restartConnectionTimeout = TimeSpan.FromSeconds(15);
	private const int RetryMaxAttempts = 5;

	private readonly SemaphoreSlim _reconnectSemaphore = new(initialCount: 1, maxCount: 1);
	private readonly string _connectionString;
	private readonly ILogger<RedisConnection> _logger;

	private long _lastReconnectTicks = DateTimeOffset.MinValue.UtcTicks;
	private DateTimeOffset _firstErrorTime = DateTimeOffset.MinValue;
	private DateTimeOffset _previousErrorTime = DateTimeOffset.MinValue;
	private ConnectionMultiplexer _connection;
	private IDatabase _database;
	private IServer _server;

	private RedisConnection(string connectionString, ILogger<RedisConnection> logger)
	{
		_connectionString = connectionString ?? throw new ArgumentNullException(nameof(connectionString));
		_logger = logger ?? throw new ArgumentNullException(nameof(logger));
	}

	public static async Task<RedisConnection> InitializeAsync(string connectionString, ILogger<RedisConnection> logger)
	{
		var redisConnection = new RedisConnection(connectionString, logger);
		await redisConnection.ForceReconnectAsync(initializing: true);
		return redisConnection;
	}

	public async Task<T> ExecuteWithRetryAsync<T>(string label, Func<IDatabase, IServer, Task<T>> func,
		CancellationToken cancellationToken = default)
	{
		var guid = Guid.NewGuid().ToString();
		var messagePrefix = $"ExecuteWithRetryAsync - {label} - {guid}";
		var executionCount = 0;
		T response = default;

		var retryStrategy = new FixedInterval(RetryMaxAttempts, TimeSpan.FromSeconds(1));
		var policy = new RetryPolicy<RedisTransientErrorDetectionStrategy>(retryStrategy);
		await policy.ExecuteAsync(async () =>
		{
			ThreadPool.GetMinThreads(out var minWorkerThreads, out var minCompletionPortThreads);

			executionCount++;
			var executionMessagePrefix =
				$"{messagePrefix} - Execution {executionCount} - Min Threads = ({minWorkerThreads}, {minCompletionPortThreads})";
			_logger.LogInformation(executionMessagePrefix);
			response = default;
			try
			{
				response = await func(_database, _server).ConfigureAwait(false);
			}
			catch (Exception e)
			{
				_logger.LogDebug($"{messagePrefix} - Execution {executionCount} - Exception = {e}");

				// any connection exceptions should trigger a force reconnect
				if (e is RedisConnectionException or SocketException or ObjectDisposedException)
				{
					try
					{
						await ForceReconnectAsync(logMessagePrefix: executionMessagePrefix);
					}
					catch (ObjectDisposedException) { }
				}

				// re-throwing will cause the policy to retry for specific types of exceptions
				throw;
			}
		}, cancellationToken).ConfigureAwait(false);

		return response;
	}

	/// <summary>
	/// Force a new ConnectionMultiplexer to be created.
	/// NOTES:
	/// 1. Users of the ConnectionMultiplexer MUST handle ObjectDisposedExceptions, which can now happen as a result of calling ForceReconnectAsync().
	/// 2. Call ForceReconnectAsync() for RedisConnectionExceptions and RedisSocketExceptions. You can also call it for RedisTimeoutExceptions,
	/// but only if you're using generous ReconnectMinInterval and ReconnectErrorThreshold. Otherwise, establishing new connections can cause
	/// a cascade failure on a server that's timing out because it's already overloaded.
	/// 3. The code will:
	/// a. wait to reconnect for at least the "ReconnectErrorThreshold" time of repeated errors before actually reconnecting
	/// b. not reconnect more frequently than configured in "ReconnectMinInterval"
	/// </summary>
	/// <param name="initializing">Should only be true when ForceReconnect is running at startup.</param>
	/// <param name="logMessagePrefix">Optional prefix for log messages</param>
	private async Task ForceReconnectAsync(bool initializing = false, string logMessagePrefix = null)
	{
		var previousTicks = Interlocked.Read(ref _lastReconnectTicks);
		var previousReconnectTime = new DateTimeOffset(previousTicks, TimeSpan.Zero);
		var elapsedSinceLastReconnect = DateTimeOffset.UtcNow - previousReconnectTime;

		// We want to limit how often we perform this top-level reconnect, so we check how long it's been since our last attempt.
		if (elapsedSinceLastReconnect < _reconnectMinInterval)
		{
			return;
		}

		var lockTaken = await _reconnectSemaphore.WaitAsync(_restartConnectionTimeout);
		if (!lockTaken)
		{
			// If we fail to enter the semaphore, then it is possible that another thread has already done so.
			// ForceReconnectAsync() can be retried while connectivity problems persist.
			return;
		}

		try
		{
			var utcNow = DateTimeOffset.UtcNow;
			previousTicks = Interlocked.Read(ref _lastReconnectTicks);
			previousReconnectTime = new DateTimeOffset(previousTicks, TimeSpan.Zero);
			elapsedSinceLastReconnect = utcNow - previousReconnectTime;

			if (_firstErrorTime == DateTimeOffset.MinValue && !initializing)
			{
				// We haven't seen an error since last reconnect, so set initial values.
				_firstErrorTime = utcNow;
				_previousErrorTime = utcNow;
				return;
			}

			if (elapsedSinceLastReconnect < _reconnectMinInterval)
			{
				return; // Some other thread made it through the check and the lock, so nothing to do.
			}

			var elapsedSinceFirstError = utcNow - _firstErrorTime;
			var elapsedSinceMostRecentError = utcNow - _previousErrorTime;

			var shouldReconnect =
				elapsedSinceFirstError >=
				_reconnectErrorThreshold // Make sure we gave the multiplexer enough time to reconnect on its own if it could.
				&& elapsedSinceMostRecentError <=
				_reconnectErrorThreshold; // Make sure we aren't working on stale data (e.g. if there was a gap in errors, don't reconnect yet).

			// Update the previousErrorTime timestamp to be now (e.g. this reconnect request).
			_previousErrorTime = utcNow;

			if (!shouldReconnect && !initializing)
			{
				return;
			}

			_firstErrorTime = DateTimeOffset.MinValue;
			_previousErrorTime = DateTimeOffset.MinValue;

			if (!initializing)
			{
				_logger.LogInformation($"{(logMessagePrefix != null ? $"{logMessagePrefix}  - " : "")}ForceReconnectAsync Start");
			}

			if (_connection != null)
			{
				try
				{
					await _connection.CloseAsync();
				}
				catch
				{
					// Ignore any errors from the old connection
				}
			}

			Interlocked.Exchange(ref _connection, null);
			var newConnection = await ConnectionMultiplexer.ConnectAsync(_connectionString);
			Interlocked.Exchange(ref _connection, newConnection);

			Interlocked.Exchange(ref _lastReconnectTicks, utcNow.UtcTicks);
			var newDatabase = _connection.GetDatabase();
			Interlocked.Exchange(ref _database, newDatabase);
			var endpoints = _connection.GetEndPoints();
			var newServer = _connection.GetServer(endpoints.First());
			Interlocked.Exchange(ref _server, newServer);

			if (!initializing)
			{
				_logger.LogInformation($"{(logMessagePrefix != null ? $"{logMessagePrefix}  - " : "")}ForceReconnectAsync Complete");
			}
		}
		finally
		{
			_reconnectSemaphore.Release();
		}
	}

	public void Dispose()
	{
		GC.SuppressFinalize(this);
		try { _connection?.Dispose(); }
		catch { }
	}
}