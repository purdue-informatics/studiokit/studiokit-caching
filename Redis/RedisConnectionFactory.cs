﻿using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Caching.Redis;

/// <summary>
/// Factory pattern implementation for Redis connections. Is constructed with a dictionary that maps shardKeys to Redis connection strings. When
/// <see cref="ExecuteWithRetryAsync{T}"/> is called, an existing <see cref="RedisConnection"/> containing a <see cref="ConnectionMultiplexer"/>
/// is returned or lazily constructed. Each <see cref="RedisConnection"/> per shard is a singleton per the StackExchange.Redis design documentation.
/// </summary>
public class RedisConnectionFactory
{
	/// <summary>
	/// Key is shardKey, value is a <see cref="RedisConnection"/> singleton. These are lazily initialized on first call to <see cref="ExecuteWithRetryAsync{T}"/>
	/// getter.
	/// </summary>
	private static readonly ConcurrentDictionary<string, RedisConnection> Connections = new();

	/// <summary>
	/// Key is shardKey, value is a Redis connection string. Provided by the constructor, used to lazily initialize a <see cref="ConnectionMultiplexer"/>
	/// per shard.
	/// </summary>
	private readonly IDictionary<string, string> _connectionStrings;

	private readonly ILoggerFactory _loggerFactory;

	/// <summary>
	/// This factory should have a singleton lifecycle.
	/// </summary>
	/// <param name="connectionStrings">A dictionary mapping shardKeys to Redis connection strings</param>
	/// <param name="loggerFactory">A factory for creating loggers for RedisConnections</param>
	public RedisConnectionFactory(IDictionary<string, string> connectionStrings, ILoggerFactory loggerFactory)
	{
		_connectionStrings = connectionStrings ?? throw new ArgumentNullException(nameof(connectionStrings));
		_loggerFactory = loggerFactory ?? throw new ArgumentNullException(nameof(loggerFactory));
	}

	private async Task<RedisConnection> GetShardConnectionAsync(string shardKey)
	{
		if (Connections.TryGetValue(shardKey, out var existingConnection))
			return existingConnection;

		// create a new connection and try adding it to the dict
		var logger = _loggerFactory.CreateLogger<RedisConnection>();
		var connection = await RedisConnection.InitializeAsync(_connectionStrings[shardKey], logger);
		if (Connections.TryAdd(shardKey, connection))
			return connection;

		// failed to add to dict, some other thread added a connection for this shardKey
		// dispose of this connection and recurse to use the other connection
		connection.Dispose();
		return await GetShardConnectionAsync(shardKey);
	}

	public async Task<T> ExecuteWithRetryAsync<T>(string shardKey, string label, Func<IDatabase, IServer, Task<T>> func,
		CancellationToken cancellationToken = default)
	{
		var connection = await GetShardConnectionAsync(shardKey);
		return await connection.ExecuteWithRetryAsync(label, func, cancellationToken);
	}
}