using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using StackExchange.Redis;
using System;
using System.Net.Sockets;

namespace StudioKit.Caching.Redis;

public class RedisTransientErrorDetectionStrategy : ITransientErrorDetectionStrategy
{
	/// <summary>
	/// Defines which types of exceptions will trigger a retry in <see cref="RedisConnection.ExecuteWithRetryAsync{T}"/>.
	/// </summary>
	public bool IsTransient(Exception ex)
	{
		return ex is RedisTimeoutException or RedisConnectionException or SocketException or ObjectDisposedException;
	}
}